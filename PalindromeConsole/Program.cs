﻿using System;
using System.Linq;
using ConsoleTables;
using PalindromeLib;

namespace PalindromeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                Console.WriteLine("Please specify at least one string to analyze.");
                Environment.Exit(-1);
            }
            
            Console.WriteLine("Analysing string/s ... (this could take a while, be patient)");

            foreach (var stringToAnalyze in args)
            {
                AnalyzeString(stringToAnalyze);
            }

            Console.WriteLine(string.Empty);
            Console.WriteLine("Cheers!");
        }

        private static void AnalyzeString(string stringToAnalyze)
        {
            var processor = new ManacherPalindromeProcessor();

            Console.WriteLine("This are the palindromes I found for");
            Console.WriteLine($"'{stringToAnalyze}'");
            Console.WriteLine(new string('*', 80));
            var table = new ConsoleTable();
            table.AddColumn(new[] { "Value", "Start", "Length" });
            foreach (var palindrome in processor.GetAllLongestPalindromes(stringToAnalyze))
            {
                var palindromeString = stringToAnalyze.Substring(palindrome.StartIndex, palindrome.Length);
                table.AddRow(palindromeString, palindrome.StartIndex, palindrome.Length);
            }
            table.Write(Format.Alternative);
        }
    }
}
