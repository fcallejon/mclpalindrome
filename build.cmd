@echo off
dotnet restore .\PalindromeLib\PalindromeLib.csproj
dotnet restore .\PalindromeConsole\PalindromeConsole.csproj
dotnet restore .\Tests\Tests.csproj

dotnet build .\PalindromeLib\PalindromeLib.csproj
dotnet build .\PalindromeConsole\PalindromeConsole.csproj
dotnet build .\Tests\Tests.csproj