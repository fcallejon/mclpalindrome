﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PalindromeLib
{
    /// <summary>
    /// Takes a string and analyze it to get the longest palindromes found.!--
    /// 
    /// This implementation uses Manacher's Longest Palindromic Substring Algorithm
    /// </summary>
    public class ManacherPalindromeProcessor
    {
        public const char StartCharacter = '^';// (char)257;
        public const char EndCharacter = '$';//(char)258;
        public const char OddEvenCharacter = '~';// (char)259;

        /// <summary>
        /// Will calculate all longest palindromes available in <paramref name="stringToAnalyze" />.
        /// </summary>
        /// <param name="stringToAnalyze"></param>
        /// <returns>A <see cref="IEnumerable{PalindromeInformation}">PalindromeInformation</see>
        /// including the bounds of the palindrome.</returns>
        public IEnumerable<PalindromeInformation> GetAllLongestPalindromes(string stringToAnalyze)
        {
            var palindromes = new List<PalindromeInformation>();
            var palindromeInfo = GetLongestPalindrome(stringToAnalyze);
            while (palindromeInfo != null)
            {
                palindromes.Add(palindromeInfo);
                palindromeInfo = GetLongestPalindrome(stringToAnalyze, palindromes);
            }

            return palindromes
                .OrderByDescending(p => p.Length)
                .ThenBy(p => p.StartIndex);
        }

        /// <summary>
        /// Will calculate the longest palindrome available in <paramref name="stringToAnalyze" />.
        /// </summary>
        /// <param name="stringToAnalyze"></param>
        /// <returns>A <see cref="PalindromeInformation"/> including the bounds of 
        /// the palindrome or null if nothing found.</returns>
        public PalindromeInformation GetLongestPalindrome(string stringToAnalyze, IEnumerable<PalindromeInformation> toIgnore = null)
        {
            toIgnore = (toIgnore ?? new PalindromeInformation[0])
                            .OrderBy(p => p.StartIndex);
            if (string.IsNullOrEmpty(stringToAnalyze))
            {
                return null;
            }

            // adding OddEvenCharacter between each char in the string
            // StartCharacter at the begining and EndCharacter at the end
            // to prevent issues with odd/even strings
            var fixedString =
                StartCharacter.ToString() +
                OddEvenCharacter.ToString() +
                string.Join(OddEvenCharacter.ToString(), stringToAnalyze.ToCharArray()) +
                OddEvenCharacter.ToString() +
                EndCharacter.ToString();

            var values = new int[fixedString.Length];
            var rightCursor = 0;
            var cursor = 0;
            var biggestCursor = 0;
            var biggestCursorCenter = 0;

            for (var index = 1; index < fixedString.Length - 1; ++index)
            {
                // jump if we got it
                var oldPalindrome = toIgnore.FirstOrDefault(p => p.StartIndex * 2 + 1 == index);
                var ignoreExtend = false;
                if (oldPalindrome != null)
                {
                    ignoreExtend = true;
                    index = oldPalindrome.StartIndex * 2 + oldPalindrome.Length * 2;
                    // fix the padding
                    if (fixedString[index] == OddEvenCharacter)
                    {
                        index++;
                    }                    
                    // restart move all as its a new fresh start
                    rightCursor = index;
                    cursor = index;
                    continue;
                }
                var leftCursor = 2 * cursor - index;

                if (index < rightCursor)
                {
                    var distanceFromIndex = rightCursor - index;
                    var leftValue = values[leftCursor];
                    values[index] = Math.Min(distanceFromIndex, leftValue);
                }

                while (!ignoreExtend && IsExtendable(fixedString, values, index))
                {
                    //increase palindrome value
                    ++values[index];

                    if (values[index] > biggestCursor)
                    {
                        biggestCursor = values[index];
                        biggestCursorCenter = index;
                    }
                }

                // in case right cursor goes off
                // we need to extend it
                if (index + values[index] > rightCursor)
                {
                    cursor = index;
                    rightCursor = index + values[index];
                }
            }

            // / 2 to remove padding
            var subtringStart = biggestCursorCenter / 2;
            subtringStart -= biggestCursor / 2;
            // ternary to avoid int rounding
            subtringStart -= (biggestCursor % 2 > 0) ? 1 : 0;

            // biggestCursor == means there is no palindromes
            // checkString to be sure there is no ReplaceCharacter
            if (biggestCursor == 1)
            {
                return null;
            }

            return new PalindromeInformation
            {
                StartIndex = subtringStart,
                Length = biggestCursor
            };
        }

        private bool IsExtendable(string processingString, int[] values, int currentCursor)
        {
            var rightCharacter = processingString[currentCursor + (1 + values[currentCursor])];
            var leftCharacter = processingString[currentCursor - (1 + values[currentCursor])];
            return leftCharacter == rightCharacter;
        }
    }
}
