using System;

namespace PalindromeLib
{
    public class PalindromeInformation
    {
        public int StartIndex { get; set; }
        public int Length { get; set; }

        public override bool Equals(object obj)
        {
            var theOther = obj as PalindromeInformation;
            if (obj == null)
            {
                return false;
            }

            return theOther.StartIndex == this.StartIndex &&
                theOther.Length == this.Length;
        }

        public override int GetHashCode()
        {
            return this.StartIndex ^ 17 + this.Length ^ 17;
        }
    }
}