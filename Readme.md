# Welcome to McLaren Palindrome Analyzer!

In order to get us going first you need to download dotnet core and install it.
To do it follow the instructions [here](https://www.microsoft.com/net/learn/get-started/windows).

# Building, Testing and Running
This app can be develop using VSCode, there is no need to get a Visual Studio (but it will work too).
VSCode can be downloaded from [here](https://code.visualstudio.com/).

> To build, test and run the analyzer there is no need to have VSCode anyway.

## Building

Open a terminal or command line and go to the folder where you have the code and then type: `.\build.cmd`

If not using Windows the commands to build are the following:

    dotnet restore ./PalindromeLib/PalindromeLib.csproj
    dotnet restore ./PalindromeConsole/PalindromeConsole.csproj
    dotnet restore ./Tests/Tests.csproj
    
    dotnet build ./PalindromeLib/PalindromeLib.csproj
    dotnet build ./PalindromeConsole/PalindromeConsole.csproj
    dotnet build ./Tests/Tests.csproj

## Testing

Run `.\tests.cmd` in the same folder.

If not using Windows: `dotnet test .\Tests\Tests.csproj`

## Running

Run in the same folder:
`.\run.cmd [THE-STRING-TO-TESTS] [THE-STRING-TO-TESTS-2] [THE-STRING-TO-TESTS-3] ...` 

If not using Windows: `dotnet run -p .\PalindromeConsole\PalindromeConsole.csproj -- [THE-STRING-TO-TESTS] ...`
