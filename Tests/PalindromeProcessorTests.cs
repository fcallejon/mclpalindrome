using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PalindromeLib;

namespace Tests
{
    [TestClass]
    public class PalindromeProcessorTests
    {
        [TestMethod]
        public void GetAllLongestPalindromes_WhenCalledWithMcLarenWordString_ShouldReturnSixResults()
        {
            var processor = new ManacherPalindromeProcessor();
            var expectedResult = new List<PalindromeInformation>(6)
            {
                new PalindromeInformation{
                    Length = 10,
                    StartIndex = 23
                },
                new PalindromeInformation{
                    Length = 8,
                    StartIndex = 13
                },
                new PalindromeInformation{
                    Length = 6,
                    StartIndex = 5
                },
                new PalindromeInformation{
                    Length = 4,
                    StartIndex = 1
                },
                new PalindromeInformation{
                    Length = 4,
                    StartIndex = 35
                },
                new PalindromeInformation{
                    Length = 3,
                    StartIndex = 40
                }
            }.ToArray();

            const string stringToAnalyze = "sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop";
            var result = processor.GetAllLongestPalindromes(stringToAnalyze).ToArray();

            Assert.IsNotNull(result);
            CollectionAssert.AllItemsAreNotNull(result);
            CollectionAssert.AllItemsAreUnique(result);
            CollectionAssert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void GetLongestPalindrome_WhenCalledWithMcLarenWordString_ShouldReturnStartIndex23Length10()
        {
            var processor = new ManacherPalindromeProcessor();
            const string stringToAnalyze = "sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop";
            var result = processor.GetLongestPalindrome(stringToAnalyze);

            Assert.IsNotNull(result);
            Assert.AreEqual(23, result.StartIndex);
            Assert.AreEqual(10, result.Length);
        }
    }
}
